using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class NewInputSystem : MonoBehaviour
{
    public InputAction playerControls;
    public Vector2 moveDirection;
    public float speed;

    public PlayerControlActions inputActions;

    void Awake()
    {
        inputActions = new PlayerControlActions();
    }


    private void OnEnable() {
        inputActions.Enable();
        /*playerControls.Enable();*/
    }

    private void OnDisable() {
        inputActions.Disable();
        /*playerControls.Disable();*/
    }


    void Update() {
        moveDirection = inputActions.playerMovement.Move.ReadValue<Vector2>();
        /*moveDirection = playerControls.ReadValue<Vector2>();*/

        if (inputActions.playerMovement.Move.IsPressed())
        {
            transform.Translate(new Vector3(moveDirection.x, 0f, moveDirection.y) * speed * Time.deltaTime);
        }

        if (inputActions.playerMovement.Jump.IsPressed())
        {
            transform.Translate(new Vector3(0f, 2f, 0f) * speed * Time.deltaTime);
        }
    }
}
